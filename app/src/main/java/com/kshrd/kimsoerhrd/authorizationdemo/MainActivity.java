package com.kshrd.kimsoerhrd.authorizationdemo;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "data";
    DatabaseReference myRef;

    EditText editText;
    Button btnSend;
    TextView textView;
    ListView listView;
    List<String> list;
    ArrayAdapter<String> arrayAdapter;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText = findViewById(R.id.edChat);
        btnSend = findViewById(R.id.btnChat);
        listView = findViewById(R.id.lvView);

        list = new ArrayList<>();
        // Write a message to the database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("message");



        arrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,list){

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

                view = super.getView(position, convertView, parent);
                if (position %2 == 1){
                    view.setBackgroundColor(Color.CYAN);
                }else {
                    view.setBackgroundColor(Color.YELLOW);
                }

                return view;
            }
        };
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveData();
                readData();
            }
        });

    }

    void saveData(){

        myRef.setValue(editText.getText().toString());
    }

    void readData(){
        // Read from the database
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);

                list.add(value);

                //textView.setText(value);
                listView.setAdapter(arrayAdapter);

                Log.d(TAG, "Value is: " + value);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }
}
